import 'package:angular_router/angular_router.dart';

import 'route_paths.dart';
import 'todo_list/todo_list_component.dart';
import 'todo_list/todo_list_component.template.dart' as todo_list_template;

export 'route_paths.dart';

class Routes {
  static final todo = RouteDefinition(
    routePath: RoutePaths.todo,
    component: todo_list_template.TodoListComponentNgFactory,
  );

  static final all = <RouteDefinition>[
    todo,
  ];
}