FROM nginx:latest
COPY ./build /www/media
COPY ./nginx.conf /etc/nginx/conf.d/default.conf