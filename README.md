# angular_exp

A web app that uses [AngularDart](https://angulardart.dev) and
[AngularDart Components](https://angulardart.dev/components).

Created from templates made available by Stagehand under a BSD-style
[license](https://github.com/dart-lang/stagehand/blob/master/LICENSE).


## Сборка проекта
webdev build


## Запуск проекта на локальной машине
webdev serve
